public class EighteenTwo {
	public static void main(String[] args) {
		int num = MyUserInput.readNumber("Enter the number: ");

		for (int i = 1; i <= num; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}

}
