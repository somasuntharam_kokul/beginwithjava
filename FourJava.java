public class FourJava {
	public static void main(String[] args) {
		int a = MyUserInput.readNumber("Enter the positive number: ");

		for (int i = 1; i <= 12; i++) {
			System.out.println(i + "x" + a + "=" + (i * a));
		}
	}

}
